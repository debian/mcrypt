# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Nikos Mavroyanopoulos
# This file is distributed under the same license as the mcrypt package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: mcrypt 2.6.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-12 16:06+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/extra.c:96
#, c-format
msgid "Keywords do not match or they are too long.\n"
msgstr ""

#: src/extra.c:111
#, c-format
msgid "Enter passphrase: "
msgstr ""

#: src/extra.c:116
#, c-format
msgid "Enter the passphrase (maximum of %d characters)\n"
msgstr ""

#: src/extra.c:120
#, c-format
msgid "Please use a combination of upper and lower case letters and numbers.\n"
msgstr ""

#: src/extra.c:187 src/extra.c:192 src/extra.c:197 src/extra.c:202
#: src/extra.c:207 src/extra.c:212
msgid "Unsupported version of encrypted file\n"
msgstr ""

#: src/extra.c:218
msgid "No Initialization vector was used.\n"
msgstr ""

#: src/extra.c:246
msgid "Salt is too long\n"
msgstr ""

#: src/extra.c:263
msgid ""
"This is a file encrypted with the 2.2 version of mcrypt. Unfortunately "
"you'll need that version to decrypt it.\n"
msgstr ""

#: src/extra.c:268
msgid ""
"This is a file encrypted with the 2.1 version of mcrypt. Unfortunately "
"you'll need that version to decrypt it.\n"
msgstr ""

#: src/extra.c:272
msgid ""
"Unable to get algorithm information. Use the --bare flag and specify the "
"algorithm manually.\n"
msgstr ""

#: src/extra.c:454
#, c-format
msgid "Signal %d caught. Exiting.\n"
msgstr ""

#: src/extra.c:466
#, c-format
msgid ""
"\n"
"Signal %d caught. Exiting.\n"
msgstr ""

#: src/extra.c:492
#, c-format
msgid "Keyfile could not be opened. Ignoring it.\n"
msgstr ""

#: src/extra.c:555
#, c-format
msgid "%s: %s already exists; do you wish to overwrite (y or n)?"
msgstr ""

#: src/extra.c:617
msgid "Could not push character back to stream\n"
msgstr ""

#: src/extra.c:625
msgid "Could not open file\n"
msgstr ""

#: src/extra.c:638
msgid "An OpenPGP encrypted file has been detected.\n"
msgstr ""

#: src/extra.c:653
#, c-format
msgid ""
"Algorithm: %s\n"
"Keysize: %d\n"
"Mode: %s\n"
"Keyword mode: %s\n"
msgstr ""

#: src/extra.c:661
#, c-format
msgid "File format: %s\n"
msgstr ""

#: src/extra.c:666
#, c-format
msgid "Input File: %s\n"
msgstr ""

#: src/extra.c:669
#, c-format
msgid "Output File: %s\n"
msgstr ""

#: src/extra.c:714
#, c-format
msgid "Time needed: "
msgstr ""

#: src/extra.c:717
#, c-format
msgid "%lu hours"
msgstr ""

#: src/extra.c:724
#, c-format
msgid "%lu minutes"
msgstr ""

#: src/extra.c:731
#, c-format
msgid "%lu seconds"
msgstr ""

#: src/extra.c:738
#, c-format
msgid "%lu milliseconds"
msgstr ""

#: src/extra.c:743
#, c-format
msgid "Wow, no time counted!\n"
msgstr ""

#: src/extra.c:750
#, c-format
msgid ""
"Processed %.3f MB, at a %.3f MB/sec rate (1 MB == 1000000 Bytes).\n"
"\n"
msgstr ""

#: src/extra.c:757
#, c-format
msgid ""
"Processed %.3f KB, at a %.3f KB/sec rate (1 KB == 1000 Bytes).\n"
"\n"
msgstr ""

#: src/mcrypt.c:79
#, c-format
msgid ""
"\n"
"Report bugs to mcrypt-dev@lists.hellug.gr.\n"
"\n"
msgstr ""

#: src/mcrypt.c:89
#, c-format
msgid ""
"\n"
"Copyright (C) 1998-2002 Nikos Mavroyanopoulos\n"
"This program is free software; you can redistribute it and/or modify \n"
"it under the terms of the GNU General Public License as published by \n"
"the Free Software Foundation; either version 2 of the License, or \n"
"(at your option) any later version. \n"
"\n"
"This program is distributed in the hope that it will be useful, \n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of \n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \n"
"GNU General Public License for more details. \n"
"\n"
"You should have received a copy of the GNU General Public License \n"
"along with this program; if not, write to the Free Software \n"
"Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n"
"\n"
msgstr ""

#: src/mcrypt.c:132
#, c-format
msgid "Mcrypt v.%s (%s-%s-%s)\n"
msgstr ""

#: src/mcrypt.c:134
#, c-format
msgid "Linked against libmcrypt v.%s\n"
msgstr ""

#: src/mcrypt.c:138
#, c-format
msgid "Copyright (C) 1998-2002 Nikos Mavroyanopoulos (nmav@gnutls.org)\n"
msgstr ""

#: src/mcrypt.c:151
msgid "The specified key size is too large for this algorithm.\n"
msgstr ""

#: src/mcrypt.c:176
msgid "The specified key size not valid for this algorithm.\n"
msgstr ""

#: src/mcrypt.c:213 src/mcrypt.c:265
msgid "Error in the arguments. Use the --help parameter for more info.\n"
msgstr ""

#: src/mcrypt.c:218
msgid "Warning: Cannot access config file.\n"
msgstr ""

#: src/mcrypt.c:234
msgid ""
"Error in the configuration file. Use the --help parameter for more info.\n"
msgstr ""

#: src/mcrypt.c:335
#, c-format
msgid "The '%s' hash algorithm was not found.\n"
msgstr ""

#: src/mcrypt.c:338
#, c-format
msgid "Use the --hashlist parameter to view all supported hashes.\n"
msgstr ""

#: src/mcrypt.c:378
msgid "Illegal compression level\n"
msgstr ""

#: src/mcrypt.c:391
#, c-format
msgid "Error in algorithm '%s'.\n"
msgstr ""

#: src/mcrypt.c:400 src/mcrypt.c:412
#, c-format
msgid "Error in mode '%s'.\n"
msgstr ""

#: src/mcrypt.c:426
msgid "Warning: It is insecure to specify keywords in the command line\n"
msgstr ""

#: src/mcrypt.c:435
msgid ""
"Warning: This system does not have a random device. Will use the included "
"entropy gatherer.\n"
msgstr ""

#: src/mcrypt.c:486
#, c-format
msgid "%s: %s is not a regular file. Skipping...\n"
msgstr ""

#: src/mcrypt.c:505
#, c-format
msgid "%s: Encrypted data will not be read from a terminal.\n"
msgstr ""

#: src/mcrypt.c:509
msgid ""
"Redirect the input instead.\n"
"Use the --help parameter for more help.\n"
msgstr ""

#: src/mcrypt.c:524
#, c-format
msgid "%s: Encrypted data will not be written to a terminal.\n"
msgstr ""

#: src/mcrypt.c:528
msgid ""
"Redirect the output instead.\n"
"Use the --help parameter for more help.\n"
msgstr ""

#: src/mcrypt.c:548
#, c-format
msgid "%s: file %s has the .nc suffix... skipping...\n"
msgstr ""

#: src/mcrypt.c:592
#, c-format
msgid "File %s was decrypted.\n"
msgstr ""

#: src/mcrypt.c:595
#, c-format
msgid "Stdin was decrypted.\n"
msgstr ""

#: src/mcrypt.c:614
#, c-format
msgid "File %s was NOT decrypted successfully.\n"
msgstr ""

#: src/mcrypt.c:624
msgid "Stdin was NOT decrypted successfully.\n"
msgstr ""

#: src/mcrypt.c:638
#, c-format
msgid "File %s was encrypted.\n"
msgstr ""

#: src/mcrypt.c:641
#, c-format
msgid "Stdin was encrypted.\n"
msgstr ""

#: src/mcrypt.c:659
#, c-format
msgid "File %s was NOT encrypted successfully.\n"
msgstr ""

#: src/mcrypt.c:669
msgid "Stdin was NOT encrypted successfully.\n"
msgstr ""

#: src/mcrypt.c:731
msgid "Unknown suffix. Will append '.dc'.\n"
msgstr ""

#: src/keys.c:131
msgid "Key transformation failed.\n"
msgstr ""

#: src/xmalloc.c:35 src/xmalloc.c:53 src/xmalloc.c:70 src/xmalloc.c:89
#: src/xmalloc.c:105 src/xmalloc.c:122 src/xmalloc.c:144
#, c-format
msgid "Cannot allocate memory\n"
msgstr ""

#: mcrypt.gaa:120
#, c-format
msgid ""
"Mcrypt encrypts and decrypts files with symmetric encryption algorithms.\n"
"Usage: mcrypt [-dFusgbhLvrzp] [-f keyfile] [-k key1 key2 ...] [-m mode] [-o "
"keymode] [-s keysize] [-a algorithm] [-c config_file] [file ...]\n"
"\n"
msgstr ""

#: mcrypt.gaa:121
msgid "Use the OpenPGP (RFC2440) file format."
msgstr ""

#: mcrypt.gaa:122
msgid "Use the native (mcrypt) file format."
msgstr ""

#: mcrypt.gaa:123 mcrypt.gaa:125
msgid "INTEGER "
msgstr ""

#: mcrypt.gaa:123
msgid "Sets the compression level for openpgp packets (0 disables)."
msgstr ""

#: mcrypt.gaa:124
msgid "decrypts."
msgstr ""

#: mcrypt.gaa:125
msgid "Set the algorithm's key size (in bytes)."
msgstr ""

#: mcrypt.gaa:126
msgid "KEYMODE "
msgstr ""

#: mcrypt.gaa:126
msgid ""
"Specify the keyword mode. Use the --list-keymodes parameter to view all "
"modes."
msgstr ""

#: mcrypt.gaa:127 mcrypt.gaa:128
msgid "FILE "
msgstr ""

#: mcrypt.gaa:127
msgid "Specify the file to read the keyword from."
msgstr ""

#: mcrypt.gaa:128
msgid "Use configuration file FILE."
msgstr ""

#: mcrypt.gaa:129
msgid "ALGORITHM "
msgstr ""

#: mcrypt.gaa:129
msgid ""
"Specify the encryption and decryption algorithm. Use the --list parameter to "
"see the supported algorithms."
msgstr ""

#: mcrypt.gaa:130 mcrypt.gaa:132
msgid "DIRECTORY "
msgstr ""

#: mcrypt.gaa:130
msgid "Set the algorithms directory."
msgstr ""

#: mcrypt.gaa:131
msgid "MODE "
msgstr ""

#: mcrypt.gaa:131
msgid ""
"Specify the encryption and decryption mode. Use the --list parameter to see "
"the supported modes."
msgstr ""

#: mcrypt.gaa:132
msgid "Set the modes directory."
msgstr ""

#: mcrypt.gaa:133
msgid "HASH "
msgstr ""

#: mcrypt.gaa:133
msgid ""
"Specify the hash algorithm to be used. Use the --list-hash parameter to view "
"the hash algorithms."
msgstr ""

#: mcrypt.gaa:134
msgid "KEY1 KEY2...KEYN "
msgstr ""

#: mcrypt.gaa:134
msgid "Specify the key(s)"
msgstr ""

#: mcrypt.gaa:135
msgid "Do not use an IV."
msgstr ""

#: mcrypt.gaa:136
msgid "Do not keep algorithm information in the encrypted file."
msgstr ""

#: mcrypt.gaa:137
msgid "Use gzip to compress files before encryption."
msgstr ""

#: mcrypt.gaa:138
msgid "Use bzip2 to compress files before encryption."
msgstr ""

#: mcrypt.gaa:139
msgid "Immediately flush the output"
msgstr ""

#: mcrypt.gaa:140
msgid "Double check passwords."
msgstr ""

#: mcrypt.gaa:141
msgid "Unlink the input file after encryption or decryption."
msgstr ""

#: mcrypt.gaa:142
msgid "Do not delete the output file if decryption failed."
msgstr ""

#: mcrypt.gaa:143
msgid "Prints timing information."
msgstr ""

#: mcrypt.gaa:144
msgid "Forces output to stdout."
msgstr ""

#: mcrypt.gaa:145
msgid "Echo asterisks when entering the password."
msgstr ""

#: mcrypt.gaa:146
msgid "Use real random data (if your system supports it)."
msgstr ""

#: mcrypt.gaa:147
msgid "Prints a list of the supported algorithms and modes."
msgstr ""

#: mcrypt.gaa:148
msgid "Prints a list of the supported key modes."
msgstr ""

#: mcrypt.gaa:149
msgid "Prints a list of the supported hash algorithms."
msgstr ""

#: mcrypt.gaa:150
msgid "More information is displayed."
msgstr ""

#: mcrypt.gaa:151
msgid "Suppress some non critical warnings."
msgstr ""

#: mcrypt.gaa:152
msgid "Prints this help"
msgstr ""

#: mcrypt.gaa:153
msgid "Prints the version number"
msgstr ""

#: mcrypt.gaa:154
msgid "Displays license information."
msgstr ""

#: src/rfc2440.c:315
#, c-format
msgid "Algorithm %s is not available in OpenPGP encryption.\n"
msgstr ""

#: src/rfc2440.c:317
#, c-format
msgid "%s will be used instead.\n"
msgstr ""

#: src/rfc2440.c:568 src/rfc2440.c:619
msgid "mhash_keygen_ext() failed.\n"
msgstr ""

#: src/rfc2440.c:571 src/rfc2440.c:622
msgid "Could not open module\n"
msgstr ""

#: src/rfc2440.c:601
#, c-format
msgid "OpenPGP: Unsupported key mode %s\n"
msgstr ""

#: src/rfc2440.c:756 src/rfc2440.c:805
msgid "mcrypt_generic_init() failed\n"
msgstr ""

#: src/rfc2440.c:768
msgid "decryption: wrong key.\n"
msgstr ""

#: src/rfc2440.c:878 src/rfc2440.c:966
#, c-format
msgid "zlib error `%s'.\n"
msgstr ""

#: src/rfc2440.c:879
#, c-format
msgid "compress: deflate returned %d.\n"
msgstr ""

#: src/rfc2440.c:967
#, c-format
msgid "uncompress: inflate returned %d.\n"
msgstr ""

#: src/rfc2440.c:999
msgid "Will decompress input file.\n"
msgstr ""

#: src/rfc2440.c:1055
msgid "Output file will be compressed.\n"
msgstr ""

#: src/rfc2440.c:1165
msgid "Unsupported OpenPGP packet!\n"
msgstr ""

#: src/popen.c:64 src/popen.c:74
#, c-format
msgid "command string is not correct!\n"
msgstr ""

#: src/popen.c:87 src/popen.c:90
#, c-format
msgid "could not create pipe\n"
msgstr ""

#: src/popen.c:95
#, c-format
msgid "could not fork child process\n"
msgstr ""

#: src/popen.c:102
#, c-format
msgid "could not close write-access of child stdin!\n"
msgstr ""

#: src/popen.c:104
#, c-format
msgid "could not dup2 child stdin!\n"
msgstr ""

#: src/popen.c:106 src/popen.c:123
#, c-format
msgid "could not close read-access of child stdin!\n"
msgstr ""

#: src/popen.c:111
#, c-format
msgid "could not close read-access of child stdout!\n"
msgstr ""

#: src/popen.c:113
#, c-format
msgid "could not dup2 child stdout!\n"
msgstr ""

#: src/popen.c:115 src/popen.c:129
#, c-format
msgid "could not close write-access of child stdout!\n"
msgstr ""

#: src/popen.c:125
#, c-format
msgid "could not fdopen child stdin pipe!\n"
msgstr ""

#: src/popen.c:131
#, c-format
msgid "could not fdopen child stdout pipe!\n"
msgstr ""

#: src/random.c:92
msgid "mhash_init() failed."
msgstr ""

#: src/random.c:122
msgid "Error while reading random data\n"
msgstr ""

#: src/random.c:146 src/classic.c:118 src/classic.c:533
msgid "Mcrypt failed to open module.\n"
msgstr ""

#: src/random.c:149
msgid "Mcrypt failed to initialize cipher.\n"
msgstr ""

#: src/rndunix.c:827
msgid "Child had pid 0!"
msgstr ""

#: src/classic.c:227
#, c-format
msgid "Error in keymode '%s'.\n"
msgstr ""

#: src/classic.c:284 src/classic.c:589
msgid "mhash initialization failed.\n"
msgstr ""

#: src/classic.c:361
msgid "Memory error\n"
msgstr ""

#: src/classic.c:513
msgid "No valid file headers found.\n"
msgstr ""

#: src/classic.c:644 src/classic.c:717
msgid "Corrupted file.\n"
msgstr ""

#: src/classic.c:663
#, c-format
msgid "Unexpected error [%d]\n"
msgstr ""

#: src/classic.c:909 src/classic.c:918
msgid "Decompressing the output file...\n"
msgstr ""

#: src/classic.c:941
#, c-format
msgid "%s check failed\n"
msgstr ""

#: src/classic.c:1054
#, c-format
msgid "Supported Hash Algorithms:\n"
msgstr ""
